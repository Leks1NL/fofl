import numpy as np
import skfuzzy as fuzz
import random as random
from skfuzzy import control as ctrl

# Membership functions:
class Triangle:
    start = 0
    top = 0
    end = 0

    def __init__(self, start, top, end):
        self.start = float(start)
        self.top = float(top)
        self.end = float(end)

    def membership(self, value):
        value = float(value)
        if value < self.start or value > self.end:
            membership = 0
        elif value == self.top:
            membership = 1
        elif value < self.top:
            membership = 1 / (self.top - self.start) * (value - self.start)
        elif value > self.top:
            membership = 1 - (1 / (self.end - self.top) * (value - self.top))
        return membership

class Trapezium:
    start = 0
    top1 = 0
    top2 = 0
    end = 0

    def __init__(self, start, top1, top2, end):
        self.start = start
        self.top1 = top1
        self.top2 = top2
        self.end = end

    def membership(self, value):
        if value < self.start or value > self.end:
            membership = 0
        elif value >= self.top1 and value <= self.top2:
            membership = 1
        elif value < self.top1:
            membership = 1 / (self.top1 - self.start) * (value - self.start)
        elif value > self.top2:
            membership = 1 - (1 / (self.end - self.top2) * (value - self.top2))
        return membership

# Calculate output using scikit fuzzy logic 
def skit(input1, input2):
    quality = ctrl.Antecedent(np.arange(0, 11, 1), 'quality')
    service = ctrl.Antecedent(np.arange(0, 11, 1), 'service')
    tip = ctrl.Consequent(np.arange(0, 26, 1), 'tip')

    quality.automf(3)
    service.automf(3)

    tip['low'] = fuzz.trimf(tip.universe, [0, 0, 13])
    tip['medium'] = fuzz.trimf(tip.universe, [0, 13, 25])
    tip['high'] = fuzz.trimf(tip.universe, [13, 25, 25])

    rule1 = ctrl.Rule(quality['poor'] | service['poor'], tip['low'])
    rule2 = ctrl.Rule(service['average'], tip['medium'])
    rule3 = ctrl.Rule(service['good'] | quality['good'], tip['high'])

    tipping_ctrl = ctrl.ControlSystem([rule1, rule2, rule3])
    tipping = ctrl.ControlSystemSimulation(tipping_ctrl)

    tipping.input['quality'] = input1
    tipping.input['service'] = input2

    tipping.compute()
    return tipping.output['tip']

# Calculates the maximum value the output MF can obtain
def rule(antecedents, inputs, operator):
    if (operator == "max"):
        val = 0
        for x in range(0, len(antecedents)):
            antecedent_input = inputs[x]
            if antecedents[x].membership(antecedent_input) > val:
                val = antecedents[x].membership(antecedent_input)
    elif (operator == "min"):
        val = 1
        for x in range(0, len(antecedents)):
            antecedent_input = inputs[x]
            if antecedents[x].membership(antecedent_input) < val:
                val = antecedents[x].membership(antecedent_input)
    return val

# Own implementation of FLS
def ownFLS(q_input, s_input):
    # Initialise MFs
    quality_poor = Triangle(0, 0, 5)
    quality_average = Triangle(0, 5, 10)
    quality_good = Triangle(5, 10, 10)

    service_poor = Triangle(0, 0, 5)
    service_average = Triangle(0, 5, 10)
    service_good = Triangle(5, 10, 10)

    tipping_low = Triangle(0, 0, 13)
    tipping_medium = Triangle(0, 13, 25)
    tipping_high = Triangle(13, 25, 25)

    # Calculate max values for the output MFs
    max_vals = [0, 0, 0]
    max_vals[0] = rule([quality_poor, service_poor], [q_input, s_input], "max")
    max_vals[1] = rule([service_average], [s_input], "max")
    max_vals[2] = rule([quality_good, service_good], [q_input, s_input], "max")

    # Aggregate rule outputs
    # Uses different implementation of "centroid function"
    # Adds points to area under the membership value and takes mean of them
    values_x = []
    for x in np.arange(0.0, 25, 0.1):
        val1 = tipping_low.membership(x)
        if val1 > max_vals[0]:
            val1 = max_vals[0]
        val2 = tipping_medium.membership(x)
        if val2 > max_vals[1]:
            val2 = max_vals[1]
        val3 = tipping_high.membership(x)
        if val3 > max_vals[2]:
            val3 = max_vals[2]
        max_val = max([val1, val2, val3])
        if max_val > 0:
            values_x.append(x)
            val = max_val-0.01
            while val > 0:
                values_x.append(x)
                val -= 0.1

    mean_x = sum(values_x) / len(values_x)
    return mean_x

# Loops over own FLS and scikit FLS and prints difference in result
def main():
    total = []
    q_inputs = []
    s_inputs = []
    cons1 = []
    cons2 = []
    for x in range(0, 100):
        q_input = random.uniform(0, 10)
        s_input = random.uniform(0, 10)

        q_inputs.append(q_input)
        s_inputs.append(s_input)

        con1 = skit(q_input, s_input)
        con2 = ownFLS(q_input, s_input)
        cons1.append(con1)
        cons2.append(con2)
        total.append(abs(con1-con2))
    
    print(max(total))
    print(sum(total)/len(total))
    
main()
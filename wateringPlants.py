# Sources:
# http://pythonhosted.org/scikit-fuzzy/user_guide.html
# http://pythonhosted.org/scikit-fuzzy/auto_examples/plot_tipping_problem_newapi.html
# http://weerlive.nl/delen.php
# https://www.buienradar.nl/overbuienradar/gratis-weerdata

import sys
import urllib.request, json
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import numpy as np
import time as tm

# Uses API to get weather data
# Adjustments to temperature / rainfall out of boundary are made to fit into membership functions
def getData():
    # Temprature
    with urllib.request.urlopen("http://weerlive.nl/api/json-10min.php?locatie=Amsterdam") as url:
        raw_data = json.loads(url.read().decode())
        data = raw_data['liveweer'][0]
        
        
    # Rainfall
    with urllib.request.urlopen("https://br-gpsgadget-new.azurewebsites.net/data/raintext?lat=51&lon=3") as url:
        rainRawValue = str(url.read()).split("b'")[1].split("|")[0]
        rainValue = 10**((float(rainRawValue)-109)/32)
        if (rainValue > 10):
            rainValue = 10
        data['rain'] = rainValue

    temp = float(data['temp'])
    if (temp < 10):
        temp = 10
    rainfall = data['rain']
    print("Temperature: " + str(temp))
    print("Rainfall: " + str(rainfall))
    return [temp, rainfall]

# Runs wateringPlants FLS
def main():
    # Input variables
    temp = ctrl.Antecedent(np.arange(10, 31, 1), "temperature") # Celsius
    rainfall = ctrl.Antecedent(np.arange(0, 11, 1), "rainfall") # mm/hour
    ground_moisture = ctrl.Antecedent(np.arange(0, 11, 1), "ground_moisture") # self-emulated

    # Output variables
    water_dose = ctrl.Consequent(np.arange(0, 201, 1), "water_dose") # mililiters
    
    # Membership functions
    temp['cold'] = fuzz.trapmf(temp.universe, [10, 10, 15, 18])
    temp['warm'] = fuzz.trapmf(temp.universe, [15, 18, 23, 26])
    temp['hot'] = fuzz.trapmf(temp.universe, [23, 26, 30, 30])

    rainfall['none'] = fuzz.trimf(rainfall.universe, [0, 0, 1])
    rainfall['light'] = fuzz.trapmf(rainfall.universe, [0, 1, 3, 4])
    rainfall['heavy'] = fuzz.trapmf(rainfall.universe, [3, 4, 10, 10])

    ground_moisture['dry'] = fuzz.trimf(ground_moisture.universe, [0, 0, 5])
    ground_moisture['medium'] = fuzz.trimf(ground_moisture.universe, [0, 5, 10])
    ground_moisture['wet'] = fuzz.trimf(ground_moisture.universe, [5, 10, 10])

    water_dose['none'] = fuzz.trimf(water_dose.universe, [0, 0, 70])
    water_dose['small'] = fuzz.trimf(water_dose.universe, [0, 70, 130])
    water_dose['normal'] = fuzz.trimf(water_dose.universe, [70, 130, 200])
    water_dose['large'] = fuzz.trimf(water_dose.universe, [130, 200, 200])

    # View MF:
    # ground_moisture['dry'].view()
    # input("Press Enter to continue...")
    # temp['cold'].view()
    # input("Press Enter to continue...")
    # rainfall['none'].view()
    # input("Press Enter to continue...")
    # water_dose['none'].view()
    # input("Press Enter to continue...")

    # Rules:
    rule1 = ctrl.Rule(temp['cold'] & rainfall['none'] & ground_moisture['dry'], water_dose['small'])
    rule2 = ctrl.Rule(temp['cold'] & ~(rainfall['none'] & ground_moisture['dry']), water_dose['none'])
    
    rule3 = ctrl.Rule(temp['warm'] & rainfall['none'] & ground_moisture['dry'], water_dose['normal'])
    rule4 = ctrl.Rule(temp['warm'] & rainfall['none'] & ground_moisture['medium'], water_dose['small'])
    rule5 = ctrl.Rule(temp['warm'] & rainfall['none'] & ground_moisture['wet'], water_dose['none'])

    rule6 = ctrl.Rule(temp['warm'] & rainfall['light'] & ground_moisture['dry'], water_dose['small'])
    rule7 = ctrl.Rule(temp['warm'] & rainfall['light'] & ~ground_moisture['dry'], water_dose['none'])
    rule8 = ctrl.Rule(temp['warm'] & rainfall['heavy'] & (~ground_moisture['dry'] | ground_moisture['dry']) , water_dose['none'])

    rule9 = ctrl.Rule(temp['hot'] & rainfall['none'] & ground_moisture['dry'], water_dose['large'])
    rule10 = ctrl.Rule(temp['hot'] & rainfall['none'] & ground_moisture['medium'], water_dose['normal'])
    rule11 = ctrl.Rule(temp['hot'] & rainfall['none'] & ground_moisture['wet'], water_dose['none'])
    rule12 = ctrl.Rule(temp['hot'] & rainfall['light'] & ground_moisture['dry'], water_dose['normal'])
    rule13 = ctrl.Rule(temp['hot'] & rainfall['light'] & ground_moisture['medium'], water_dose['small'])
    rule14 = ctrl.Rule(temp['hot'] & rainfall['light'] & ground_moisture['wet'], water_dose['none'])
    rule15 = ctrl.Rule(temp['hot'] & rainfall['heavy'] & (~ground_moisture['dry'] | ground_moisture['dry']) , water_dose['none'])

    # View rules:
    # rule1.view()

    # Control system
    water_dose_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10,
                                          rule11, rule12, rule13, rule14, rule15])

    # Simulation
    water_dose_sim = ctrl.ControlSystemSimulation(water_dose_ctrl)

    # Input values:
    # data = getData()
    # For practical purposes, the data is filled in by hand instead of using the API
    water_dose_sim.input['temperature'] = float(sys.argv[1])
    water_dose_sim.input['rainfall'] = float(sys.argv[2])
    water_dose_sim.input['ground_moisture'] = float(sys.argv[3])

    water_dose_sim.compute()

    print(water_dose_sim.output['water_dose'])
    water_dose.view(sim=water_dose_sim)
    input("Press Enter to continue...")

main()